# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2025-01-28 18:47+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. ChangelogURI
#: ../data/templates/ubuntu.info.in.h:4
#, no-c-format
msgid "http://changelogs.ubuntu.com/changelogs/pool/%s/%s/%s/%s_%s/changelog"
msgstr ""

#. Description
#: ../data/templates/ubuntu.info.in:33
msgid "Ubuntu development series"
msgstr ""

#. CompDescriptionLong
#: ../data/templates/ubuntu.info.in:112
msgid "Canonical-supported free and open-source software"
msgstr ""

#. CompDescription
#: ../data/templates/ubuntu.info.in:114
msgid "Community-maintained"
msgstr ""

#. CompDescriptionLong
#: ../data/templates/ubuntu.info.in:115
msgid "Community-maintained free and open-source software"
msgstr ""

#. CompDescription
#: ../data/templates/ubuntu.info.in:117
msgid "Non-free drivers"
msgstr ""

#. CompDescriptionLong
#: ../data/templates/ubuntu.info.in:118
msgid "Proprietary drivers for devices"
msgstr ""

#. CompDescription
#: ../data/templates/ubuntu.info.in:121
msgid "Restricted software"
msgstr ""

#. CompDescriptionLong
#: ../data/templates/ubuntu.info.in:122
msgid "Software restricted by copyright or legal issues"
msgstr ""

#. Description
#: ../data/templates/ubuntu.info.in:129
msgid "Ubuntu {version} '{codename}'"
msgstr ""

#. Description
#: ../data/templates/ubuntu.info.in:137
msgid "Installation medium with Ubuntu {version} '{codename}'"
msgstr ""

#. CompDescription
#: ../data/templates/ubuntu.info.in:140 ../data/templates/debian.info.in:113
msgid "Officially supported"
msgstr ""

#. CompDescription
#: ../data/templates/ubuntu.info.in:142
msgid "Restricted copyright"
msgstr ""

#. Description
#: ../data/templates/ubuntu.info.in:149
msgid "Canonical Partners"
msgstr ""

#. CompDescription
#: ../data/templates/ubuntu.info.in:151
msgid "Software packaged by Canonical for their partners"
msgstr ""

#. CompDescriptionLong
#: ../data/templates/ubuntu.info.in:152
msgid "This software is not part of Ubuntu."
msgstr ""

#. Description
#: ../data/templates/ubuntu.info.in:160
msgid "Independent"
msgstr ""

#. CompDescription
#: ../data/templates/ubuntu.info.in:162
msgid "Provided by third-party software developers"
msgstr ""

#. CompDescriptionLong
#: ../data/templates/ubuntu.info.in:163
msgid "Software offered by third party developers."
msgstr ""

#. Description
#: ../data/templates/ubuntu.info.in:182
msgid "Important security updates"
msgstr ""

#. Description
#: ../data/templates/ubuntu.info.in:195 ../data/templates/debian.info.in:53
msgid "Recommended updates"
msgstr ""

#. Description
#: ../data/templates/ubuntu.info.in:208
msgid "Pre-released updates"
msgstr ""

#. Description
#: ../data/templates/ubuntu.info.in:221
msgid "Unsupported updates"
msgstr ""

#. ChangelogURI
#: ../data/templates/debian.info.in.h:4
#, no-c-format
msgid ""
"https://metadata.ftp-master.debian.org/changelogs/%s/%s/%s/%s_%s_changelog"
msgstr ""

#. Description
#: ../data/templates/debian.info.in:25
msgid "Debian {version} '{codename}'"
msgstr ""

#. Description
#: ../data/templates/debian.info.in:47
msgid "Security updates"
msgstr ""

#. Description
#: ../data/templates/debian.info.in:59
msgid "Proposed updates"
msgstr ""

#. Description
#: ../data/templates/debian.info.in:66
msgid "Debian current stable release"
msgstr ""

#. Description
#: ../data/templates/debian.info.in:81
msgid "Debian testing"
msgstr ""

#. Description
#: ../data/templates/debian.info.in:111
msgid "Debian 'Sid' (unstable)"
msgstr ""

#. CompDescription
#: ../data/templates/debian.info.in:115
msgid "DFSG-compatible Software with Non-Free Dependencies"
msgstr ""

#. CompDescription
#: ../data/templates/debian.info.in:117
msgid "Non-DFSG-compatible Firmware for Hardware Support"
msgstr ""

#. CompDescription
#: ../data/templates/debian.info.in:119
msgid "Non-DFSG-compatible Software"
msgstr ""

#. TRANSLATORS: %s is a country
#: ../aptsources/distro.py:209 ../aptsources/distro.py:477
#, python-format
msgid "Server for %s"
msgstr ""

#. More than one server is used. Since we don't handle this case
#. in the user interface we set "custom servers" to true and
#. append a list of all used servers
#: ../aptsources/distro.py:228 ../aptsources/distro.py:240
#: ../aptsources/distro.py:261
msgid "Main server"
msgstr ""

#: ../aptsources/distro.py:270
msgid "Custom servers"
msgstr ""

#: ../apt/package.py:550
#, python-format
msgid "Missing description for '%s'.Please report."
msgstr ""

#: ../apt/package.py:560
#, python-format
msgid "Invalid unicode in description for '%s' (%s). Please report."
msgstr ""

#: ../apt/package.py:1220 ../apt/package.py:1236 ../apt/package.py:1349
#: ../apt/package.py:1365
msgid "The list of changes is not available"
msgstr ""

#: ../apt/package.py:1357
#, python-format
msgid ""
"The list of changes is not available yet.\n"
"\n"
"Please use http://launchpad.net/ubuntu/+source/%s/%s/+changelog\n"
"until the changes become available or try again later."
msgstr ""

#: ../apt/package.py:1372
msgid ""
"Failed to download the list of changes. \n"
"Please check your Internet connection."
msgstr ""

#: ../apt/debfile.py:91
#, python-format
msgid "List of files for '%s' could not be read"
msgstr ""

#: ../apt/debfile.py:102
#, python-format
msgid "List of control files for '%s' could not be read"
msgstr ""

#: ../apt/debfile.py:235
#, python-format
msgid "Dependency is not satisfiable: %s\n"
msgstr ""

#: ../apt/debfile.py:264
#, python-format
msgid "Conflicts with the installed package '%s'"
msgstr ""

#: ../apt/debfile.py:418
#, python-format
msgid ""
"Breaks existing package '%(pkgname)s' dependency %(depname)s "
"(%(deprelation)s %(depversion)s)"
msgstr ""

#: ../apt/debfile.py:449
#, python-format
msgid ""
"Breaks existing package '%(pkgname)s' conflict: %(targetpkg)s (%(comptype)s "
"%(targetver)s)"
msgstr ""

#: ../apt/debfile.py:466
#, python-format
msgid ""
"Breaks existing package '%(pkgname)s' that conflict: '%(targetpkg)s'. But "
"the '%(debfile)s' provides it via: '%(provides)s'"
msgstr ""

#: ../apt/debfile.py:525
msgid "No Architecture field in the package"
msgstr ""

#: ../apt/debfile.py:537
#, python-format
msgid ""
"Wrong architecture '%s' -- Run dpkg --add-architecture to add it and update "
"afterwards"
msgstr ""

#. the deb is older than the installed
#: ../apt/debfile.py:552
msgid "A later version is already installed"
msgstr ""

#: ../apt/debfile.py:578
msgid "Failed to satisfy all dependencies (broken cache)"
msgstr ""

#: ../apt/debfile.py:607
#, python-format
msgid "Cannot install '%s'"
msgstr ""

#: ../apt/debfile.py:688
msgid ""
"Automatically decompressed:\n"
"\n"
msgstr ""

#: ../apt/debfile.py:694
msgid "Automatically converted to printable ascii:\n"
msgstr ""

#: ../apt/debfile.py:800
#, python-format
msgid "Install Build-Dependencies for source package '%s' that builds %s\n"
msgstr ""

#: ../apt/debfile.py:813
msgid "An essential package would be removed"
msgstr ""

#: ../apt/progress/text.py:87
#, python-format
msgid "%c%s... Done"
msgstr ""

#: ../apt/progress/text.py:129
msgid "Hit "
msgstr ""

#: ../apt/progress/text.py:138
msgid "Ign "
msgstr ""

#: ../apt/progress/text.py:140
msgid "Err "
msgstr ""

#: ../apt/progress/text.py:151
msgid "Get:"
msgstr ""

#: ../apt/progress/text.py:221
msgid " [Working]"
msgstr ""

#: ../apt/progress/text.py:234
#, python-format
msgid ""
"Media change: please insert the disc labeled\n"
" '%s'\n"
"in the drive '%s' and press enter\n"
msgstr ""

#: ../apt/progress/text.py:248
#, python-format
msgid "Fetched %sB in %s (%sB/s)\n"
msgstr ""

#: ../apt/progress/text.py:271
msgid "Please provide a name for this medium, such as 'Debian 2.1r1 Disk 1'"
msgstr ""

#: ../apt/progress/text.py:290
msgid "Please insert an installation medium and press enter"
msgstr ""
